﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Binding_Text_Lab
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void onSliderChange(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            cLabel.FontSize = value;
        }

        void onRedClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            cLabel.TextColor = button.BackgroundColor;
        }
        void onBlueClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            cLabel.TextColor = button.BackgroundColor;
        }
        void onYellowClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            cLabel.TextColor = button.BackgroundColor;
        }

        
    }
}
